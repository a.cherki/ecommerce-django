import json

from django.shortcuts import render, redirect
from .models import *
from django.http import JsonResponse
import datetime
from .utils import *
from django.contrib.auth.forms import UserCreationForm
from .forms import CreateUserForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages


def store(request):
    data = cartData(request)
    cartItems = data['cartItems']

    products = Product.objects.all()

    context = {'products': products, 'cartItems': cartItems}
    return render(request, 'store/store.html', context)


def cart(request):
    data = cartData(request)
    cartItems = data['cartItems']
    order = data['order']
    items = data['items']

    context = {'items': items, 'order': order, 'cartItems': cartItems, }
    return render(request, 'store/cart.html', context)


def checkout(request):
    data = cartData(request)
    cartItems = data['cartItems']
    order = data['order']
    items = data['items']
    user = request.user

    context = {'items': items, 'order': order, 'cartItems': cartItems}

    return render(request, 'store/checkout.html', context)


def updateItem(request):
    data = json.loads(request.body)
    productId = data['productId']
    action = data['action']

    customer = request.user.customer
    product = Product.objects.get(id=productId)
    order, created = Order.objects.get_or_create(customer=customer, complete=False)

    orderItem, created = OrderItem.objects.get_or_create(order=order, product=product)

    if action == 'add':
        orderItem.quantity = (orderItem.quantity + 1)
    elif action == 'remove':
        orderItem.quantity = (orderItem.quantity - 1)

    orderItem.save()

    if orderItem.quantity <= 0:
        orderItem.delete()

    return JsonResponse('Item was added', safe=False)


def processOrder(request):
    transaction_id = datetime.datetime.now().timestamp()
    data = json.loads(request.body)
    if request.user.is_authenticated:
        customer = request.user.customer
        order, created = Order.objects.get_or_create(customer=customer, complete=False)

    else:
        customer, order = guestOrder(request, data)

    total = float(data['form']['total'])
    order.transaction_id = transaction_id

    if data['shipping']['address'] is None:
        order.address = customer.address
    else:
        order.address = data['shipping']['address']

    if total == order.get_cart_total:
        order.complete = True

    order.save()

    # ShippingAddress.objects.create(
    #     customer=customer,
    #     order=order,
    #     address=data['shipping']['address'],
    # )

    return JsonResponse('payment complete', safe=False)


def register_page(request):
    if request.user.is_authenticated:
        return redirect('store')
    else:
        form = CreateUserForm()
        if request.method == 'POST':
            form = CreateUserForm(request.POST)
            if form.is_valid():
                user = form.save()
                username = request.POST.get('username')
                email = request.POST.get('email')
                customer = Customer.objects.get_or_create(user=user, name=username, email=email)
                return redirect('login')

    context = {'form': form}
    return render(request, 'store/register.html', context)


def login_page(request):
    if request.user.is_authenticated:
        return redirect('store')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('store')
            else:
                messages.info(request, 'Username OR password incorrect')
    context = {}
    return render(request, 'store/login.html', context)


def logout_user(request):
    logout(request)
    return redirect('store')


def account(request):
    user = request.user
    customer = Customer.objects.get(user=user)
    print(customer.email)
    # address = ShippingAddress.objects.get(customer=customer)
    context = {'customer': customer}
    return render(request, 'store/account.html', context)


def product_details(request, product_id):
    product = Product.objects.get(id=product_id)
    # images = Image.objects.filter(product=product)
    context = {'product': product}
    return render(request, 'store/ProductDetails.html', context)
