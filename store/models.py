from django.db import models
from django.contrib.auth.models import User
from multiselectfield import MultiSelectField
from colorfield.fields import ColorField

# Create your models here.
sizes = (("S", "S"), ("M", "M"), ("L", "L"))


class Customer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(max_length=200, null=True)
    email = models.CharField(max_length=200, null=True)
    address = models.CharField(max_length=200, null=True)
    phone = models.CharField(max_length=15, default='0554970929')
    image = models.ImageField(null=True, blank=True)

    def __str__(self):
        return self.name or ''

    @property
    def imageURL(self):
        try:
            url = self.image.url
        except:
            url = ''
        return url


class Product(models.Model):
    name = models.CharField(max_length=200)
    price = models.DecimalField(max_digits=7, decimal_places=2)
    size = MultiSelectField(choices=sizes, max_length=200, null=True)
    # color = ColorField(MultiSelectField(default='#FF0000'))
    description = models.TextField(null=True)
    # digital = models.BooleanField(default=False, null=True, blank=True)  # if product is physical we do need
    # to ship it if product is digital we don't need to ship it

    image = models.ImageField(null=True, blank=True)

    def __str__(self):
        return self.name or ''

    @property
    def imageURL(self):
        try:
            url = self.image.url
        except:
            url = ''
        return url


# class Image(models.Model):
#     name = models.CharField(max_length=255)
#     product = models.ForeignKey(Product, on_delete=models.CASCADE)
#     image = models.ImageField(null=True)
#     default = models.BooleanField(default=False)
#
#     def __str__(self):
#         return self.name or ''
#
#     @property
#     def imageURL(self):
#         try:
#             url = self.image.url
#         except:
#             url = ''
#         return url
#

class Order(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.SET_NULL, null=True, blank=True)
    date_ordered = models.DateTimeField(auto_now_add=True)
    complete = models.BooleanField(default=False)
    transaction_id = models.CharField(max_length=100, null=True)
    address = models.CharField(max_length=200, null=True)
    phone = models.CharField(max_length=15, default='')

    # order_items = models.ManyToManyField(OrderItem)

    def __str__(self):
        return str(self.id) or ''

    @property
    def get_cart_total(self):
        orderitems = self.orderitem_set.all()
        total = sum([item.get_total for item in orderitems])
        return total

    @property
    def get_cart_items(self):
        orderitems = self.orderitem_set.all()
        total = sum([item.quantity for item in orderitems])
        return total


class OrderItem(models.Model):
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True)
    order = models.ForeignKey(Order, on_delete=models.SET_NULL, null=True)
    quantity = models.IntegerField(default=0, null=True, blank=True)
    date_added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.id) or ''

    @property
    def get_total(self):
        total = self.product.price * self.quantity
        return total

# class ShippingAddress(models.Model):
#     customer = models.ForeignKey(Customer, on_delete=models.SET_NULL, null=True)
#     order = models.ForeignKey(Order, on_delete=models.SET_NULL, null=True)
#     address = models.CharField(max_length=200, null=False)
#     date_added = models.DateTimeField(auto_now_add=True)

# def __str__(self):
#     return self.address
